var grpc = require("grpc");
var protos = require("../bin/greeter_service_pb");
var services = require("../bin/greeter_service_grpc_pb");

function greet(call, callback) {
    var response = new protos.GreetResponse();
    console.log("[REQUEST]");
    console.log(call.request.getName());
    response.setMessage('Hello ' + call.request.getName());
    callback(null, response);
}

function main() {
    var server = new grpc.Server();
    server.addService(services.GreeterServiceService, {
        greet: greet
    });
    server.bind('0.0.0.0:2020', grpc.ServerCredentials.createInsecure());
    server.start();
}

main();
