var grpc = require("grpc");
var protos = require("../bin/greeter_service_pb");
var services = require("../bin/greeter_service_grpc_pb");

function main() {
    var client = new services.GreeterServiceClient('0.0.0.0:2020', grpc.credentials.createInsecure());
    var request = new protos.GreetRequest();
    request.setName("gmgrl");
    client.greet(request, function (err, response) {
        if (err == null) {
            console.log("[SUCCESS] Response#messeage: ")
            console.log(response.getMessage());
        } else {
            console.log("[ERROR]")
            console.log(err);
        }
    });
}

main();
